#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
BACKUP_DIR="$DIR/._backup`date +%Y%m%d-%s`"

echo "[INFO] Backing up files to $BACKUP_DIR ..."

# Backup any existing files.
mkdir -v $BACKUP_DIR

# Copy these files since they will be modified.
[[ -e ~/.bash_profile ]] && cp ~/.bash_profile $BACKUP_DIR/.bash_profile
[[ -e ~/.bashrc ]] && cp ~/.bashrc $BACKUP_DIR/.bashrc

# Move these files since they will be replaced.
mv -fv ~/.git-autocompletion.sh $BACKUP_DIR/
mv -fv ~/.gitattributes $BACKUP_DIR/
mv -fv ~/.gitconfig $BACKUP_DIR/
mv -fv ~/.gitignore $BACKUP_DIR/

echo
echo "Modifying shell configuration startup scripts"
grep -q -F '.dotfiles_bash_profile' ~/.bash_profile 2>/dev/null || echo 'source ~/.dotfiles_bash_profile' >> ~/.bash_profile
grep -q -F '.dotfiles_bashrc' ~/.bashrc 2>/dev/null || echo 'source ~/.dotfiles_bashrc' >> ~/.bashrc

echo
echo "[INFO] Symlinking environment"

# symlink in the new dotfiles
ln -fsv $DIR/bash/bash_profile ~/.dotfiles_bash_profile
ln -fsv $DIR/bash/bashrc ~/.dotfiles_bashrc
ln -fsv $DIR/git/git-autocompletion.sh ~/.git-autocompletion.sh
ln -fsv $DIR/git/gitattributes ~/.gitattributes
ln -fsv $DIR/git/gitconfig ~/.gitconfig
ln -fsv $DIR/git/gitignore ~/.gitignore
