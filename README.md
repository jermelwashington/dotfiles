**Get up and running in under 2 minutes**

---

## dotfiles
Yet another dotfiles repo with useful defaults for bash and git.

---

## Before installing
THIS WILL MODIFY AND OVERWRITE YOUR FILES! However backups of your files are stored in `._backupYYYYMMDD-epoch`

[Read the install.sh script][0] before running it to make sure it is not going to do anything you don't want it to do.

---

## How to install.

    $ git clone https://jermelwashington@bitbucket.org/jermelwashington/dotfiles.git ~/.dotfiles
    $ cd ~/.dotfiles
    $ source ./install.sh


[0]: https://bitbucket.org/jermelwashington/dotfiles/src/master/install.sh
